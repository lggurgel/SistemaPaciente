#ifndef MEDICO_HPP
#define MEDICO_HPP

#include "Pessoa.hpp"

class Medico : public Pessoa{

public:
	Medico();
	Medico(string nome);
	~Medico();


};
#endif