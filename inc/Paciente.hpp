#ifndef PACIENTE_HPP
#define PACIENTE_HPP

#include <string>
#include "Pessoa.hpp"
#include "Medico.hpp"
#include "Visitante.hpp"

class Paciente : public Pessoa{

private:
	Medico* medico;

public:
	Paciente();
	Paciente(string nomePaciente, string nomeMedico);
	~Paciente();

	string cadastraPaciente();
	string getMedico();
	void setMedico(string nomeMedico); 

};
#endif