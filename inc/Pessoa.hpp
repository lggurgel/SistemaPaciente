#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <string>

using namespace std;

class Pessoa{

protected:
	string nome;

public: 
	Pessoa(); 
	~Pessoa();

	string getNome();
	void setNome(string nome);

};
#endif