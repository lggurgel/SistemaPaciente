#ifndef VISITANTE_HPP
#define VISITANTE_HPP

#include "Pessoa.hpp"

class Visitante : public Pessoa{

private:
	string grau;

public:
	Visitante();
	~Visitante();

	

	string getGrau();
	void setGrau(string grau);


};
#endif