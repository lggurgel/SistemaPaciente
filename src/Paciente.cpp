#include "../inc/Paciente.hpp"
#include <string>
#include <fstream>

using namespace std;

Paciente::Paciente(){
	nome = "";
}

Paciente::Paciente(string nomePaciente, string nomeMedico){
	nome = nomePaciente;
	medico = new Medico(nomeMedico);
}


Paciente::~Paciente(){}

string Paciente::cadastraPaciente(){

	ofstream cadastra;
	string mensagem;

	cadastra.open("doc/dados.txt", ofstream::app);

	if(!cadastra.is_open()){
		mensagem = "Falha ao cadastrar.";
	}else{

		cadastra << nome << endl;
		cadastra << medico->getNome();
		cadastra << endl << endl;


		mensagem = "Cadastro realizado.";

	}

	return mensagem;

}

string Paciente::getMedico(){

	return medico->getNome();

}

void Paciente::setMedico(string nomeMedico){

	medico->setNome(nomeMedico);

}