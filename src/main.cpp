#include "../inc/Paciente.hpp"
#include <iostream>
#include <fstream>

using namespace std;

int main(){

	void consultarPaciente();
	void formularioPaciente();


	cout << "SISTEMA HOSPITAL OO" << endl << endl;
	cout << "(1)Consultar Paciente  (2)Cadastrar Paciente" << endl;
	cout << "Entre com operação: ";

	int menu;
	cin >> menu;
	
	switch(menu){
		case 1:{
			consultarPaciente();
			break;
		}
		case 2:{
			cin.ignore();
			formularioPaciente();
			break;
		}
		default:
			cout << "Opção inválida" << endl;
			break;

	}



	return 0;
}

void consultarPaciente(){

	ifstream consulta("doc/dados.txt", ifstream::in);

	string dados;

	while (getline(consulta, dados)){

		if (dados == "")continue;

		cout << "Paciente: " + dados << endl;

		getline(consulta, dados);
		cout << "Médico: " + dados << endl << endl;

	}
}

void formularioPaciente(){

	string nomePaciente, nomeMedico;

	cout << "Nome do paciente: ";
	getline(cin, nomePaciente);

	cout << "Nome do medico: ";
	getline(cin, nomeMedico);

	Paciente paciente(nomePaciente, nomeMedico);

	cout << paciente.cadastraPaciente() << endl;

}